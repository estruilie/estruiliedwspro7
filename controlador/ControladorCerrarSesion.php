<?php
if( !isset($_SESSION) ) session_start();  
    unset($_SESSION["logueado"]);

session_destroy();
header("Location: ../vista/login.php");
?>

