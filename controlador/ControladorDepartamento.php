<?php
if (strlen(session_id()) < 1)
    session_start();
error_reporting(E_ALL);
ini_set('display_errors','1');

include_once "funciones.php";
include_once "../modelo/ModeloFichero.php";

function mostrarDepartamentos() {
    if ( $_SESSION['modelo']==1 )
        $modelo = new ModeloFichero();
    else 
        $modelo = new ModeloMysql();
    $departamentos = $modelo->leerDepartamentos();

    if ( $departamentos[0] != null) {

        foreach ($departamentos as $d) {
            echo "<tr>";
            echo "<td>" . $d->getId() . "</td>";
            echo "<td>" . $d->getNombre() . "</td>";
            echo "<td>" . $d->getUbicacion() . "</td>";
            echo "</tr>";
        }
    }
}

function guardarDepartamento() {
    if ( isset($_REQUEST["id"]) && isset($_REQUEST["nombre"]) && isset($_REQUEST["ubicacion"]) ) {
        $id = recoge("id");
        $nombre = recoge("nombre");
        $ubicacion = recoge("ubicacion");
   
        $departamento = new Departamento($id, $nombre, $ubicacion);

        if ( $_SESSION['modelo']==1 )
            $modelo = new ModeloFichero();
        else 
            $modelo = new ModeloMysql();
        $grabado = $modelo->guardarDepartamento($departamento);
        
        if ( $grabado === false ) 
            header("Location: ../vista/VistaOperacionNoOK.php");
        
        else
            header("Location: ../vista/VistaOperacionOK.php");
    }
}

function nuevoIdDep() {
    if ( $_SESSION['modelo']==1 )
        $modelo = new ModeloFichero();
    else
        $modelo = new ModeloMysql();
    $todos = $modelo->leerDepartamentos();
    $id = count($todos) + 1;
        
    echo $id;
}


?>    