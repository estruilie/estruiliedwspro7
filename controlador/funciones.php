<?php
include_once '../modelo/ModeloMysql.php';

/* Función no utilizada
$campos = array("id","nombre","ubicacion","departamento");
function recogeCampo($var, $var2){
    global $campos;
    foreach($campos as $campo) {
        if (isset($_REQUEST[$var]) && $_REQUEST[$var] == $campo) 
            return $campo;
    }
    return $var2;
}
*/

function recoge($campo) {
    if (isset($_REQUEST[$campo])) 
        $valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
    return $valor;
}

function obtenerModelo() {
    if ( isset($_REQUEST["modelo"]) ) {
        session_start();  // iniciamos la sesión para almacenar el modelo escogido para la aplicación
        $_SESSION['modelo'] = recoge('modelo');
        if ( $_SESSION['modelo'] == 1 )
            header('Location: ../vista/index2.php');
        else
            header('Location: ../vista/indexBBDD.php');
    }
}

function opcionBBDD() {
    if ( isset($_REQUEST["instalar"]) ) {

        $_REQUEST["instalar"] = recoge('instalar');
        if ( $_REQUEST["instalar"] == 1 ) {
            $modelo = new ModeloMysql();
            $modelo->instalarBD();
            header('Location: ../vista/VistaInstalacionBBDD.php');
        }
        else
            header('Location: ../vista/index2.php');
    }   
}


function mostrarPagina() {
// Si el usuario no está logueado esta función redirigirá a la página de login    
    if ( $_SESSION["logueado"]!="logueado" )
        header('Location: ../vista/login.php');
}


?>