<?php 
    require "../modelo/config.php";
    require "../controlador/funciones.php";
    if ( !isset($_SESSION) ) session_start ();
    mostrarPagina();
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Instalación BBDD</title>
        <link rel="stylesheet" href="../css/index.css">
        <link rel="stylesheet" href="../css/estilos.css">
    </head>
    <body>
        <?php include "header.php"; ?>

        <div class="centrar">
            <h1>Instalando la base de datos MySql</h1>
            <hr><br/><br/>
            <p><strong> &CircleDot;&nbsp;Creada BBDD: <?php echo Config::$bdnombre; ?> </strong></p>
            <p><strong> &CircleDot;&nbsp;Creada tabla: Departamento </strong></p>
            <p><strong> &CircleDot;&nbsp;Creada tabla: Trabajador </strong></p>
            <p><strong> &CircleDot;&nbsp;Creada CAj: Trabajador.departamento -> Departamento.id  </strong></p>
        </div>
        <?php include "footer.php"; ?>
    </body>
</html>