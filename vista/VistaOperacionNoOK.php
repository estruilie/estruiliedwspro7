<?php 
    require "../modelo/config.php";
    include_once "../controlador/funciones.php";
    if ( !isset($_SESSION) ) session_start ();
    mostrarPagina();
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Operación no ejecutada</title>
        <link rel="stylesheet" href="../css/index.css">
        <link rel="stylesheet" href="../css/estilos.css">
    </head>
    <body>
        <?php include "header.php"; ?>
        
        <h2>Sucedió un error al realizar la operación</h2>
        <h3>Sugerencia: compruebe los permisos de escritura</h3>
        
        <?php include "footer.php"; ?>
    </body>
</html>