<?php 
require "../modelo/config.php"; 
require "../controlador/funciones.php";
if ( !isset($_SESSION) ) session_start ();
mostrarPagina();
?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Evaluable T7</title>
        <link rel="stylesheet" href="../css/index.css">
    </head>
    <body>
        <?php include "header.php"; ?>
        
        <div class="centrar">
            <h1>Escoge una opción para almacenar los datos</h1><hr/><br/>
            <form method="POST" action="#" onsubmit="<?php obtenerModelo();?>" >
                <input type="radio" name="modelo" value="1" >Modelo con ficheros
                <br/>
                <input type="radio" name="modelo" value="2" checked="checked" >Modelo con base de datos MySql
                <br/><br/><br/>
                <input type="submit" name="entrar" value="Entrar en la aplicación" />
                <br/>
                 
            </form>
       </div>
        <?php include "footer.php"; ?>
    </body>
</html>