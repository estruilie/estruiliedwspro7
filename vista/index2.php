<?php 
    require "../modelo/config.php";    
    require "../controlador/funciones.php";
    if ( !isset($_SESSION) ) session_start ();
    mostrarPagina();
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Evaluable T7 - Gestión de datos</title>
        <link rel="stylesheet" href="../css/index.css">
    </head>
    <body>
        <?php include "header.php"; ?>
        <h2> ELIGE UNA OPCIÓN: </h2>
        <div class="opciones">
            <a href="VistaDepartamentos.php"><h2>GESTIONAR DEPARTAMENTOS</h2></a>
        </div>
        <div class="opciones">
            <a href="VistaTrabajadores.php"><h2>GESTIONAR TRABAJADORES</h2></a>
        </div>
        <?php include "footer.php"; ?>
    </body>
</html>
